﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IOF.XML.V3;
using Newtonsoft.Json;

namespace WOC.StartLists.Presenter {
  class Program {
    static void Main(string[] args) {
      var settings = new Settings {
        Out = "summary.csv",
        StartLists = new List<StartListSetting> {
          new StartListSetting { Name = "QB.1", Path = @"C:\Work\QB9-Final\qb.1-start.xml"},
          new StartListSetting { Name = "QB.2", Path = @"C:\Work\QB9-Final\qb.2-start.xml"},
          new StartListSetting { Name = "QB.4", Path = @"C:\Work\QB9-Final\qb.4-start.xml"},
          new StartListSetting { Name = "QB.5", Path = @"C:\Work\QB9-Final\qb.5-start.xml"},
        }
      };

      File.WriteAllText("settings.json", JsonConvert.SerializeObject(settings, Formatting.Indented));

      var setting = JsonConvert.DeserializeObject<Settings>(File.ReadAllText("settings.json"));

      var mappings = new Dictionary<string, StartSummary>();

      foreach(var startListSetting in setting.StartLists) {
        XmlSerializer serializer = new XmlSerializer(typeof(StartList));

        using (StreamReader reader = new StreamReader(startListSetting.Path)) {
          var sl = serializer.Deserialize(reader) as StartList;

          foreach(var classStart in sl.ClassStart) {
            foreach (var personStart in classStart.PersonStart) {
              string firstName = personStart.Person?.Name?.Given;
              string lastname = personStart.Person?.Name?.Family;
              string club = personStart.Organisation?.Name;

              string key = $"{firstName} {lastname} {club}";

              if (!mappings.ContainsKey(key)) {
                mappings.Add(key, new StartSummary { Club = club, Firstname = firstName, LastName = lastname, Items = new List<StartItem>() });
              }

              if (personStart.Start != null && personStart.Start.Length > 0) {
                var start = personStart.Start[0];
                var si = start.ControlCard != null && start.ControlCard.Length > 0 ? start.ControlCard[0].Value : "";

                mappings[key].Items.Add(new StartItem {
                  EventName = startListSetting.Name,
                  Grade = classStart.Class.Name,
                  SI = si,
                  Time = start.StartTimeSpecified ? start.StartTime.ToString("HH:mm") : ""
                });
              }
            }
          }
        }
      }

      var ordered = mappings.Values
        .OrderBy(p => p.LastName)
        .ThenBy(p => p.Firstname)
        .ThenBy(p => p.Club);

      var builder = new StringBuilder();

      string sep = ",";
      var row = "";

      foreach (var e in setting.StartLists) {
          row += $"{sep}\"{e.Name} - Class\"{sep}\"{e.Name} - SI\"{sep}\"{e.Name} - Start\"";
      }

      builder.AppendLine($"\"Lastname\"{sep}\"First name\"{sep}\"Club\"{row}");

      foreach(var item in ordered) {
        row = "";

        foreach (var e in setting.StartLists) {
          var r = item.Items.FirstOrDefault(p => p.EventName == e.Name);

          if (r == null) {
            row += $"{sep}{sep}{sep}";
          } else {
            row += $"{sep}\"{r.Grade}\"{sep}\"{r.SI}\"{sep}\"{r.Time}\"";
          }
        }

        builder.AppendLine($"\"{item.LastName}\"{sep}\"{item.Firstname}\"{sep}\"{item.Club}\"{row}");
      }

      File.WriteAllText(setting.Out, builder.ToString());
      Console.WriteLine("Done!");
    }
  }

  public class StartSummary {
    public string Firstname { get; set; }
    public string LastName { get; set; }
    public string Club { get; set; }
    public List<StartItem> Items { get; set; }
  }

  public class StartItem {
    public string Time { get; set; }
    public string Grade { get; set; }
    public string SI { get; set; }
    public string EventName { get; set; }
  }

  public class Settings {
    public List<StartListSetting> StartLists { get; set; }
    public string Out { get; set; }
  }

  public class StartListSetting {
    public string Path { get; set; }
    public string Name { get; set; }
  }
}
